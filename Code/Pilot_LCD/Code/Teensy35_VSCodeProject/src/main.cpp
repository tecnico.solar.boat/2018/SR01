/*
    Copyright (C) 2018  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat  
*/


//#include "lcd_serial.h"
#include "lcd_print.h"
#include <Wire.h>
#include <TimeLib.h>

IntervalTimer TimeTimer, LedTimer, WarningTimer;
LiquidCrystal tsb_lcd(TWILCD_DEFAULT_ADDR);
bool LEDstate=true;
tsb_serial_msg_t tsb_msg;
TSB_SERIAL tsb_serial;
TSB_PRINT tsb_print;

void led_timer()
{
	tsb_print.heartbeat_blink();
}

void warning_timer()
{
	tsb_print.warning_blink();
}

void time_timer()
{
	if(tsb_print.reset_screen_cnt++ % 10) tsb_print.reset_var_lcd(tsb_lcd);
	tsb_print.update_time(tsb_lcd);
}

void setup(void) {
	// Set LED pins as outputs
	pinMode(LEDPIN_RED1, OUTPUT);
	pinMode(LEDPIN_GRN1, OUTPUT);
	pinMode(LEDPIN_RED2, OUTPUT);
	pinMode(LEDPIN_GRN2, OUTPUT);
	pinMode(LEDPIN_RED3, OUTPUT);
	pinMode(LEDPIN_GRN3, OUTPUT);
	pinMode(BUZZERPIN, OUTPUT);

	// Initialize LCD
	Wire.begin();
	Serial.begin(115200);
	tsb_lcd.begin(LCD_COLUMNS, LCD_ROWS);
	tsb_lcd.createChar(0, tsb_print.customChar);

	// Print Logos and StartMsg in LCD
	tsb_print.init_logos(tsb_lcd);

	// Start LED heartbeat
	LedTimer.begin(led_timer, 10000000);

	// Wait here until RPi gives time
	while(Serial.available() <= 0);
	tsb_msg = tsb_serial.receive_msg();
	setTime(tsb_msg.data[0],tsb_msg.data[1],tsb_msg.data[2],tsb_msg.data[3],tsb_msg.data[4],2000+tsb_msg.data[5]);

	//delay(5000);
	//setTime(16,17,44,21,4,2008);

	// Prepare LCD for first use and update time
	tsb_print.reset_var_lcd(tsb_lcd);
	tsb_print.update_time(tsb_lcd);

	// Everything set, start time timer
	TimeTimer.begin(time_timer, 1000000);
}

void loop(void)
{
	// Check if a valid message is received and print it to the LCD
	tsb_msg = tsb_serial.receive_msg();
	//tsb_print.print_msg( tsb_lcd,tsb_msg);

	if(tsb_msg.valid_msg == true){
		switch (tsb_msg.data_id)
		{
			case MOTORMSG_ID:
				tsb_serial.decode_motormsg(tsb_msg);
				tsb_print.update_mctrl(tsb_lcd, tsb_serial.Power_Input.phy, tsb_serial.RPM.phy, tsb_serial.TempMos.phy);

				//if(tsb_serial.MotorWarnings != 0) WarningTimer.begin(warning_timer, 100000);
				//else WarningTimer.end();

				break;

			case FOILSMSG_ID:
				tsb_serial.decode_foilsmsg(tsb_msg);
				tsb_print.update_fctrl(tsb_lcd, tsb_serial.speed);
				break;

			case BMSMSG_ID:
				tsb_serial.decode_bmsmsg(tsb_msg);
				tsb_print.update_bctrl(tsb_lcd, tsb_serial.BatPower.phy, tsb_serial.SolarPower.phy,
					tsb_serial.VBat.phy, tsb_serial.BatStatus, tsb_serial.BatWarnings);

				//if(tsb_serial.BatWarnings != 0) WarningTimer.begin(warning_timer, 100000);
				//else WarningTimer.end();

				break;
		}
	}
	//delay(1000);
}
