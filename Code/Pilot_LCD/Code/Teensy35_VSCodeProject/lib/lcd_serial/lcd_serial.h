/*
    Copyright (C) 2018  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat  
*/

// -------------------------------------------------------------
// tsb_serial.h
// -------------------------------------------------------------

#include <Arduino.h>

#define MSG_START_BYTE		0xAC
#define MSG_END_BYTE		0xAD
#define MSG_DATA_OFFSET		3		// Byte offset where data starts in a message
#define MSG_NODATA_SIZE		5		// Size of a message without any data
#define MSG_MAXDATA_SIZE	16		// Let's assume a maximum of 16 bytes of data
#define MSG_MAX_SIZE		MSG_NODATA_SIZE + MSG_MAXDATA_SIZE

#define HEARTBEAT_ADDR	0x00
#define MOTOR_CTRL_ADDR	0x01
#define FOIL_CTRL_ADDR	0x02
#define BAT_CTRL_ADDR	0x04
#define LCD_CTRL_ADDR	0x08

// LCD controller related message IDs
#define MOTORMSG_ID		0x02
#define	MOTORMSG_SIZE 	8
#define FOILSMSG_ID		0x03
#define FOILSMSG_SIZE	8
#define BMSMSG_ID		0x04
#define BMSMSG_SIZE		10



typedef struct tsb_serial_msg {
	bool valid_msg;
	uint8_t addr;
	uint8_t	data_id;
	uint8_t data[MSG_MAXDATA_SIZE];
	uint8_t data_size;
} tsb_serial_msg_t;

typedef struct tsb_lcd_var
{
	int32_t raw; // raw value
	float phy;	// real value
} tsb_lcd_var_t;

// -------------------------------------------------------------
class TSB_SERIAL
{
	public:
		// MOTORMSG_ID
		tsb_lcd_var_t Power_Input;
		tsb_lcd_var_t RPM;
		tsb_lcd_var_t TempMos;
		//uint8_t MotorWarnings = 1;

		// BMSMSG_ID
		tsb_lcd_var_t VBat;
		tsb_lcd_var_t BatPower, SolarPower;
		uint8_t BatStatus, BatWarnings;

		// FOILSMSG_ID
		tsb_lcd_var_t speedX, speedY;
		float speed;

		void send_msg(tsb_serial_msg_t msg);
		tsb_serial_msg_t receive_msg();

		void decode_motormsg(tsb_serial_msg_t msg);
		void decode_foilsmsg(tsb_serial_msg_t msg);
		void decode_bmsmsg(tsb_serial_msg_t msg);
};
