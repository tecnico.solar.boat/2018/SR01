/*
    Copyright (C) 2018  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat 
*/

#include <Arduino.h>
#include <tsb_serial.h>

/* Defines of the pins*/
#define ignition_pin 15
#define gate_mos 21
#define MS0 2
#define MS1 3
#define MS2 4
#define MS3 5
#define VSPA 22
#define led 13

/* Defines for the power of the solar Painels*/
#define solaroffset 520
#define span 0.0465

/* Define of the heartbeat rate*/
#define hb_rate 5

/* Define of the warnings*/
#define warning_temperature 55
#define warning_current 200
#define warning_overcellvolt 4.3
#define warning_undercellvolt 3.0


/* Define for the thermistors (temperature)*/
#define R01 10000.0f //resistencia do termistor a 25ºC
#define R02 47000.0f //resistencia do termistor a 25ºC
#define Rref 10000.0f //R1 in in voltage divider
#define Vref 3.3f //Tensao de alimentaçao do termistor
#define Beta1 3960.0f //Beta do termistor
#define Beta2 3984.0f //Beta do termistor
#define RMUX 70.0f // Resistencia interna do MUX
#define T0 298.15 //reference Temperature


/* Define for the solar voltages*/
#define Refsolarbot 680.0f
#define Refsolartop 10000.0f

class TSB_BSS{

    public:
    String status;
    float voltage;
    float current;
    float soc;
    float *temperatures;
    float *cellsvoltages;
    float *solar_volt;
    int warning;
    int solarpower;
    tsb_serial_msg_t message_status_soc_volt_curr_solar_warning;
    tsb_serial_msg_t message_temperatures1;
    tsb_serial_msg_t message_temperatures2;
    tsb_serial_msg_t message_cellvoltages1;
    tsb_serial_msg_t message_cellvoltages2;
    tsb_serial_msg_t message_solarvolt;

    TSB_BSS();
    ~TSB_BSS();
    void SerialPrint();
    void get_status();
    void get_voltage();
    void get_current();
    void get_stateofcharge();
    void get_temperatures();
    void get_cellsvoltages();
    void check_warnings();
    void send_Message();
    void get_solarpower();
    void readTemperatures(float R0, float Beta, int num);
    void get_solarvoltages();
    String createData();
    
};

float Ieee754converter(const char s[32]);
int binary_to_decimal(char *bin);
char *decimal_to_binary(int n);
void send_Heartbeat();


