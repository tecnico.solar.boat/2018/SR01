/*
    Copyright (C) 2018  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat 
*/

/*
 * TSB
 * Code for the communication between the BMS 
 * and the teensy.
 * 
 */
#include "TSB_BSS.h"


/* Auxiliar vector for the temperatures*/
int v1[16]={0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1};
int v2[16]={0,0,1,1,0,0,1,1,0,0,1,1,0,0,1,1};
int v3[16]={0,0,0,0,1,1,1,1,0,0,0,0,1,1,1,1};
int v4[16]={0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1};

/*
 * Function to convert from binary (char)
 * to float using ieee754 converion
 * 
 */
float Ieee754converter(const char s[32])
{
  u_int32_t v;
  float f;
  unsigned i;
  char *p1 = (char*)&v , *p2 = (char*)&f;

  v = 0;
  for (i = 0; i < 32; i++)
    v = (v << 1) + (s[i] - '0');

  for (i = 0; i < sizeof(f); i++)
    *p2++ = *p1++;

  return f;
}


/*
 * Function to convert from binary
 * to decimal
 * 
 */
int binary_to_decimal(char *bin) 
{
  int i=0, j;
  j = sizeof(int)*8;
  while ( (j--) && ((*bin=='0') || (*bin=='1')) ) {
      i <<= 1;
      if ( *bin=='1' ) i++;
      bin++;
  }
  return i;
}


/*
 * Function to convert from decimal
 * to binary
 * 
 */
char *decimal_to_binary(int n)
{
  int c, d, count;
  char *pointer;

  count = 0;
  pointer = (char*)malloc(32+1);

  if (pointer == NULL)
    exit(EXIT_FAILURE);

  for (c = 31 ; c >= 0 ; c--)
  {
    d = n >> c;

    if (d & 1)
        *(pointer+count) = 1 + '0';
    else
        *(pointer+count) = 0 + '0';

    count++;
  }
  *(pointer+count) = '\0';

  return  pointer;
}

/*
 * Function to send Heartbeat message
 * 
 */
void send_Heartbeat(){
  TSB_SERIAL tsb_serial_class;
  tsb_serial_msg_t message_HeartBeat;
  message_HeartBeat.addr=0x03;
  message_HeartBeat.data_id=0x00;
  message_HeartBeat.data_size=0x00;
  tsb_serial_class.send_msg(message_HeartBeat);
}

//--------------------------------------------------------------------------------------------

/*
 * Constructor of the class
 * 
 */
TSB_BSS::TSB_BSS() {
  temperatures = new float[32]();
  cellsvoltages = new float[12]();
  solar_volt = new float[5]();

  message_status_soc_volt_curr_solar_warning.addr=0x03;
  message_status_soc_volt_curr_solar_warning.data_id=0x01;
  message_status_soc_volt_curr_solar_warning.data_size=16;

  message_temperatures1.addr=0x03;
  message_temperatures1.data_id=0x02;
  message_temperatures1.data_size=16;

  message_temperatures2.addr=0x03;
  message_temperatures2.data_id=0x03;
  message_temperatures2.data_size=16;

  message_cellvoltages1.addr=0x03;
  message_cellvoltages1.data_id=0x04;
  message_cellvoltages1.data_size=12;
  
  message_cellvoltages2.addr=0x03;
  message_cellvoltages2.data_id=0x05;
  message_cellvoltages2.data_size=12;

  message_solarvolt.addr=0x03;
  message_solarvolt.data_id=0x06;
  message_solarvolt.data_size=10;


}

/*
 * Destructor of the class
 * 
 */
TSB_BSS::~TSB_BSS() {
  delete[] this->temperatures;
  delete[] this->cellsvoltages;
  delete[] this->solar_volt;
}



/*
 * Function to get the status
 * of the Battery
 * 
 */
void TSB_BSS::get_status(){
    byte message[] = {0xAA,0x18,0x7F,0x1A}; 
    Serial1.write(message, sizeof(message));
	  u_int8_t rbyte;
    u_int8_t out[8]; 
    int i=0;
    delay(50);
	  while (Serial1.available()) {
      rbyte = Serial1.read(); 
      out[i]=rbyte;
      i++; 
    }
    if ( out[2] == 145) //hex 91
		this->status="Charging";
	  else if( out[2] == 146 ) //92
		this->status="Fully Charged";	
	  else if( out[2] == 147 ) //93
		this->status="Discharging";	
	  else if( out[2] == 149 ) //95
		this->status="Sleep";
	  else if( out[2] == 150 ) //96
		this->status="Regeneration";	
	  else if( out[2] == 151 ) //97
		this->status="Idle";
	  else if( out[2] == 155 ) //9b
		this->status="Fault";    
    
    this->message_status_soc_volt_curr_solar_warning.data[0] = out[2];           	
}


/*
 * Function to get the
 * voltage of the Battery
 * 
 */
void TSB_BSS::get_voltage(){
    byte message[] = {0xAA,0x14,0x7F,0x1F}; 
	  uint8_t rbyte;
    uint8_t out[8] = {};
    int i=0;
    int bms_message; 


    Serial1.write(message, sizeof(message));
    delay(50);
	  while (Serial1.available()) {   
      rbyte = Serial1.read();
      out[i]=rbyte;
      i++; 
    }
    bms_message = (out[5]<<24) | (out[4]<<16)| (out[3]<<8) | out[2];

    this->voltage = *(float*)&bms_message;

    this->message_status_soc_volt_curr_solar_warning.data[5] = out[2];
    this->message_status_soc_volt_curr_solar_warning.data[6] = out[3];
    this->message_status_soc_volt_curr_solar_warning.data[7] = out[4];
    this->message_status_soc_volt_curr_solar_warning.data[8] = out[5];
}

/*
 * Function to get the
 * current of the Battery
 * 
 */
void TSB_BSS::get_current(){
    byte message[] = {0xAA,0x15,0xBE,0xDF};    
	  uint8_t rbyte;
    uint8_t out[8] = {};
    int i=0;
    int bms_message; 

    Serial1.write(message, sizeof(message));
    delay(50);
	  while (Serial1.available()) {   
      rbyte = Serial1.read(); 
      out[i]=rbyte;
      i++; 
    }
    bms_message = (out[5]<<24) | (out[4]<<16)| (out[3]<<8) | out[2];
    this->current = *(float*)&bms_message;

    this->message_status_soc_volt_curr_solar_warning.data[9] = out[2];
    this->message_status_soc_volt_curr_solar_warning.data[10] = out[3];
    this->message_status_soc_volt_curr_solar_warning.data[11] = out[4];
    this->message_status_soc_volt_curr_solar_warning.data[12] = out[5];
}


/*
 * Function to get the
 * state of charge of the Battery
 * 
 */
void TSB_BSS::get_stateofcharge(){
    byte message[] = {0xAA,0x1A,0xFE,0xDB}; 
    Serial1.write(message, sizeof(message));
	  uint8_t rbyte;
    uint8_t out[8] = {};
    int i=0;
    int bms_message; 

    delay(50);
	  while (Serial1.available()) {   
      rbyte = Serial1.read(); 
      out[i]=rbyte;
      i++; 
    }
    bms_message = (out[5]<<24) | (out[4]<<16)| (out[3]<<8) | out[2];
    this->soc = (float) bms_message/1000000;

    this->message_status_soc_volt_curr_solar_warning.data[1] = out[2];
    this->message_status_soc_volt_curr_solar_warning.data[2] = out[3];
    this->message_status_soc_volt_curr_solar_warning.data[3] = out[4];
    this->message_status_soc_volt_curr_solar_warning.data[4] = out[5];
}

/*
 * Function to get the
 * temperatures of the Battery
 * including the ones from the BMS
 * 
 */
void TSB_BSS::get_temperatures(){
    byte message[] = {0xAA,0x1B,0x3F,0x1B}; 
    Serial1.write(message, sizeof(message));
	  uint8_t rbyte;
    uint8_t out[10] = {};
    int i=0;
    int bms_message; 

    delay(50);
	  while (Serial1.available()) {   
      rbyte = Serial1.read(); 
      out[i]=rbyte;
      i++; 
    }
    bms_message=(out[4]<<8) | out[3];
    this->temperatures[0] = (float) bms_message/10;
    bms_message=(out[6]<<8) | out[5];
    this->temperatures[1] = (float) bms_message/10;
    bms_message=(out[8]<<8) | out[7];
    this->temperatures[2] = (float) bms_message/10;

    this->readTemperatures(R01,Beta1,1);
    this->readTemperatures(R02,Beta2,2);

}

/*
 * Function to get the
 * cells voltages of the Battery
 * 
 */
void TSB_BSS::get_cellsvoltages(){
    byte message[] = {0xAA,0x1C,0x7E,0xD9}; 
    Serial1.write(message, sizeof(message));
	  uint8_t rbyte;
    uint8_t out[29] = {};
    int i=0, j=0;
    int bms_message; 

    delay(50);
	  while (Serial1.available()) {   
      rbyte = Serial1.read(); 
      out[i]=rbyte;
      i++; 
    }

    for(int i=0; i<26 ;i=i+2){
      bms_message=(out[i+4]<<8) | out[i+3];
      this->cellsvoltages[j] = (float) bms_message/10000;
      if(i<12){
        this->message_cellvoltages1.data[i]=out[i+3];
        this->message_cellvoltages1.data[i+1]=out[i+4];
      }
      else{
        this->message_cellvoltages2.data[i-12]=out[i+3];
        this->message_cellvoltages2.data[i-11]=out[i+4];
      }
      
      j++;
    }
}


/*
 * Function to check all the
 * possible warnings to send
 * 
 */
void TSB_BSS::check_warnings(){
  this->warning=0;
  if(this->current>warning_current)
    this->warning|=1;
  for(int i=0; i<32; i++){
    if(i!=3 && i!=14 && i!=15 && i!=22 && i!=23 && i!=30 && i!=31)
    if(this->temperatures[i]>warning_temperature){
      this->warning|=2;
    }  
  } 
  for(int i=0; i<12; i++){
    if(this->cellsvoltages[i]>warning_overcellvolt){
      this->warning|=4;
      digitalWrite(gate_mos, LOW);
    } 
    //else digitalWrite(gate_mos, HIGH);
    if(this->cellsvoltages[i]<warning_undercellvolt){
      this->warning|=8;
    } 
  } 
  if(digitalRead(ignition_pin))
    this->warning|=16;
  if(digitalRead(gate_mos))
    this->warning|=32;
  
  this->message_status_soc_volt_curr_solar_warning.data[15]=this->warning;
}

/*
 * Function to read the temperatures
 * from the thermistors,
 * depending on the MUX chosen
 * 
 */
void TSB_BSS::readTemperatures(float R0, float Beta, int num){
  for(int i=0; i<16;i++){
      int sensorValue=0;
      if(v1[i]==1)  digitalWriteFast(MS0, HIGH);
      else if (v1[i]==0) digitalWriteFast(MS0, LOW);
      if(v2[i]==1)  digitalWriteFast(MS1, HIGH);
      else if(v2[i]==0) digitalWriteFast(MS1, LOW);
      if(v3[i]==1)  digitalWriteFast(MS2, HIGH);
      else if (v3[i]==0) digitalWriteFast(MS2, LOW);
      if(v4[i]==1)  digitalWriteFast(MS3, HIGH);
      else if (v4[i]==0)digitalWriteFast(MS3, LOW);
      delayMicroseconds(100);
      if(num==1) sensorValue = analogRead(A10);
      else if(num==2) sensorValue = analogRead(A11);
      // Convert the analog reading (which goes from 0 - 4096) to a voltage (0 - 3.3V):
      float voltage_out = sensorValue * (Vref / 4096.0);
      if(num==2) voltage_out += 0.05; // added offset on the 47k thermistor
      float Rthermistor = (Rref*(voltage_out/Vref))/(1-(voltage_out/Vref)); //voltage divider equation solved for R2
      int temperature = Beta/(log(Rthermistor/(R0*exp(-Beta/T0)))) - 273.15;
      if(num==1){
        this->temperatures[i]=temperature;
        this->message_temperatures1.data[i]=temperature;
        if(i==3 || i==14 || i==15){
          this->message_temperatures1.data[i]=0;
          this->temperatures[i]=0;
        } 
      } 
      else if(num==2){
        this->message_temperatures2.data[i]=temperature;
        this->temperatures[i+16]=temperature;
        if(i==6 || i==7 || i==14 || i==15){
          this->message_temperatures2.data[i]=0;
          this->temperatures[i+16]=0;
        } 
      } 
      
  }
}

/*
 * Function to get the 
 * voltages from the solar painels
 * 
 */
void TSB_BSS::get_solarvoltages(){
      int sv[5]={};
      sv[0] = analogRead(A2);
      sv[1] = analogRead(A3);
      sv[2] = analogRead(A4);
      sv[3] = analogRead(A5);
      sv[4] = analogRead(A6);

      float voltage=0;
      float current=0;
      int j=0;
      for(int i=0;i<5;i++){
        voltage = sv[i] * (Vref / 4096.0);
        current = voltage/Refsolarbot;
        this->solar_volt[i] = current*(Refsolarbot+Refsolartop);
        int aux = current*(Refsolarbot+Refsolartop)*100;
        this->message_solarvolt.data[j]=lowByte(aux);
        this->message_solarvolt.data[j+1]=highByte(aux);
        j=j+2;
    }    
}

/*
 * Function to get the 
 * power from the solar painels
 * 
 */
void TSB_BSS::get_solarpower(){
      float reading = analogRead(A8);
      float current = (reading - solaroffset)*(Vref/4096.0)/span;
      this->solarpower=current*this->voltage;
      if(abs(this->solarpower)<100) this->solarpower=0;
      this->message_status_soc_volt_curr_solar_warning.data[13]=lowByte(this->solarpower);
      this->message_status_soc_volt_curr_solar_warning.data[14]=highByte(this->solarpower);
}

/*
 * Function to send
 * all the messages to the RPI
 * 
 */
void TSB_BSS::send_Message(){
  TSB_SERIAL tsb_serial_class;
  tsb_serial_class.send_msg(this->message_status_soc_volt_curr_solar_warning);
  tsb_serial_class.send_msg(this->message_temperatures1); 
  tsb_serial_class.send_msg(this->message_temperatures2);
  tsb_serial_class.send_msg(this->message_cellvoltages1);
  tsb_serial_class.send_msg(this->message_cellvoltages2);
  tsb_serial_class.send_msg(this->message_solarvolt);
}


String TSB_BSS::createData(){
  String Data;
  Data=String(this->status)+";"+String(this->soc)+";"+String(this->voltage)+";"+String(this->current);
  for(int i=0; i<32; i++){
    Data+=";"+String(this->temperatures[i]);
  }
  for(int i=0; i<12; i++){
    Data+=";"+String(this->cellsvoltages[i]);
  } 
  for(int i=0;i<=4;i++){
    Data+=";"+String(this->solar_volt[i]);
  }
  Data+=";"+String(this->solarpower);
  return Data;
}

/*
 * Function to print all the
 * info to the serial
 * (DEBUGGING ONLY)
 * 
 */
void TSB_BSS::SerialPrint(){
  Serial.print("Status: ");
  Serial.println(this->status);
  Serial.print("State of Charge: ");
  Serial.println(this->soc);
  Serial.print("Voltage: ");
  Serial.println(this->voltage);
  Serial.print("Current: ");
  Serial.println(this->current);
  Serial.print("Temperatures ");
  for(int i=0; i<32; i++){
    Serial.print(i);
    Serial.print(": ");
    Serial.print(this->temperatures[i]);
    Serial.print("°C ");
  }
  Serial.println();
  Serial.print("Cell voltages ");
  for(int i=0; i<12; i++){
    Serial.print(i);
    Serial.print(": ");
    Serial.print(this->cellsvoltages[i]);
    Serial.print("V ");
  } 
  Serial.println();
  Serial.print("Solar Voltage ");
  for(int i=0;i<5;i++){
    Serial.print(i);
    Serial.print(": ");
    Serial.print(this->solar_volt[i]);
    Serial.print("V ");
  }
  Serial.println();
  Serial.print("Solar Power: ");
  Serial.print(this->solarpower); 
  Serial.println(" W");  
}