/*
    Copyright (C) 2018  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat 
*/


/*
 * TSB
 * Code for the SD data logger 
 * for every eletrical system
 * 
 */

#include "TSB_datalogger.h"

SdFat sd;
SdFile file;
String Data="";
int entryId = 0;
String timeString;
String dateString;
String logname;
IntervalTimer SD_timer;
bool sd_insert = true;

void getTime()
{
  String minutes;
  String hours;
  String seconds;
  String month_;
  String mday;
  
  if(minute()<10) minutes = "0"+String(minute());
  else minutes = String(minute());
  
  if(second()<10) seconds = "0"+String(second());
  else seconds = String(second());

  if(hour()<10) hours = "0"+String(hour());
  else hours = String(hour());

  if(month()<10) month_ = "0"+String(month());
  else month_ = String(month());

  if(day()<10) mday = "0"+String(day());
  else mday = String(day());

  timeString = hours+":"+minutes+":"+seconds;
  dateString = mday+"/"+month_+"/"+String(year());
}

 void writeLogEntry()
{
  if (sd_insert == false) return;
  file.open(logname.c_str(),  O_WRITE | O_AT_END);
  String logEntry;
  getTime();
  logEntry = String(entryId)+";"+timeString+";"+dateString+";"+Data;
  entryId ++;
  file.println(logEntry);
  file.close();
} 

void initLOG()
{
  if (sd_insert == false) return;
  file.open(logname.c_str(), O_WRITE | O_CREAT | O_AT_END );
  /*file.println("----------------Data logging----------------");
  file.println(String(System_Name)+" at "+timeString+"-"+dateString);
  file.println("--------------------------------------------");*/
  file.println("BMS;Time;Date;Status;SOC;Voltage;Current;T1;T2;T3;T4;T5;T6;T7;T8;T9;T10;T11;T12;T13;T14;T15;T16;T17;T18;T19;T20;T21;T22;T23;T24;T25;T26;T27;T28;T29;T30;T31;T32;V1;V2;V3;V4;V5;V6;V7;V8;V9;V10;V11;V12;SolarVolt1;SolarVolt2;SolarVolt3;SolarVolt4;SolarVolt5;SolarPower");
  file.close();
}

void initSD(){
  if (!sd.begin()) {
    sd_insert = false;
  }
  else{
    sd.chvol();  
    getTime();
    logname=System_Name+String(day())+"_"+String(month())+"-"+String(hour())+"_"+String(minute())+".txt";
  }
  
}