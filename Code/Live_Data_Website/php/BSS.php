<?php
/*
	Copyright (C) 2018  Técnico Solar Boat

	This program is free software: you can redistribute 
	it and/or modify it under the terms of the GNU General Public License 
	as published by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat 
*/

$DB_NAME = 'YOUT INFO HERE';
$DB_HOST = 'YOUT INFO HERE';
$DB_USER = 'YOUT INFO HERE';
$DB_PASS = 'YOUT INFO HERE';
$mysqli = new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);

if (mysqli_connect_errno()) {
	printf("Connect failed: %s\n", mysqli_connect_error());
	exit();
}


$result = $mysqli->query('SELECT * FROM BSS ORDER By ID DESC LIMIT 1;');
if ($result->num_rows > 0) {
 	$row = $result->fetch_assoc();
    $table = [
    		'BMS_Status' => $row['BMS_Status'],
    		'SOC' => $row['SOC'], 
		  	'Voltage' => $row['Voltage'],
		  	'Current' => $row['Current'],
		 	'V1' => $row['V1'],
			'V2' => $row['V2'],
			'V3' => $row['V3'],
			'V4' => $row['V4'],
			'V5' => $row['V5'],
			'V6'=> $row['V6'],
			'V7' => $row['V7'],
			'V8' => $row['V8'],
			'V9'=> $row['V9'],
			'V10'=> $row['V10'],
			'V11'=> $row['V11'],
			'V12'=> $row['V12'],
			'T1' => $row['T1'],
			'T2' => $row['T2'],
			'T3' => $row['T3'],
			'T4' => $row['T4'],
			'T5' => $row['T5'],
			'T6'=> $row['T6'],
			'T7' => $row['T7'],
			'T8' => $row['T8'],
			'T9'=> $row['T9'],
			'T10'=> $row['T10'],
			'T11'=> $row['T11'],
			'T12'=> $row['T12'],
			'T13' => $row['T13'],
			'T14' => $row['T14'],
			'T15' => $row['T15'],
			'T16' => $row['T16'],
			'T17' => $row['T17'],
			'T18'=> $row['T18'],
			'T19' => $row['T19'],
			'T20' => $row['T20'],
			'T21'=> $row['T21'],
			'T22'=> $row['T22'],
			'T23'=> $row['T23'],
			'T24' => $row['T24'],
			'T25'=> $row['T25'],
			'T26'=> $row['T26'],
			'T27'=> $row['T27'],
			'T28' => $row['T28'],
			'T29'=> $row['T29'],
			'T30'=> $row['T30'],
			'T31'=> $row['T31'],
			'T32' => $row['T32'],
			'V_Painel1' => $row['V_Painel1'],
			'V_Painel2' => $row['V_Painel2'],
			'V_Painel3' => $row['V_Painel3'],
			'V_Painel4' => $row['V_Painel4'],
			'V_Painel5' => $row['V_Painel5'],
			'SOLAR' => $row['SOLAR'],
			'Warnings' => $row['Warnings'],
			'time'=> $row['time'],
			'id'=> $row['id']];
}
else {
	$table = null;
}
echo json_encode($table);
?>