<?php
/*
	Copyright (C) 2018  Técnico Solar Boat

	This program is free software: you can redistribute 
	it and/or modify it under the terms of the GNU General Public License 
	as published by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat 
*/

$DB_NAME = 'YOUR INFO HERE';
$DB_HOST = 'YOUR INFO HERE';
$DB_USER = 'YOUR INFO HERE';
$DB_PASS = 'YOUR INFO HERE';
$mysqli = new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);

if (mysqli_connect_errno()) {
	printf("Connect failed: %s\n", mysqli_connect_error());
	exit();
}
$result = $mysqli->query('SELECT T_Bat, time FROM TSB ORDER By ID ASC;');
if ($result->num_rows > 0) {
	// output data of each row
	while($row = $result->fetch_assoc()) {
		$table[] = array("T_Bat" => floatval($row["T_Bat"]), 'time'=> $row['time']);
	}
}
else {
	$table = null;
}
echo json_encode($table, JSON_FORCE_OBJECT);
?>