<?php
/*
	Copyright (C) 2018  Técnico Solar Boat

	This program is free software: you can redistribute 
	it and/or modify it under the terms of the GNU General Public License 
	as published by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat 
*/

$DB_NAME = 'YOU INFO HERE';
$DB_HOST = 'YOUT INFO HERE';
$DB_USER = 'YOU INFO HERE';
$DB_PASS = 'YOU INFO HERE';
$mysqli = new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);

if (mysqli_connect_errno()) {
	printf("Connect failed: %s\n", mysqli_connect_error());
	exit();
}


$result = $mysqli->query('SELECT * FROM Motor ORDER By ID DESC LIMIT 1;');
if ($result->num_rows > 0) {
 	$row = $result->fetch_assoc();
    $table = [
			'VoltInput' => $row['VoltInput'],
			'RPM'=> $row['RPM'],
			'PowerInput' => $row['PowerInput'],
			'MosTemp1' => $row['MosTemp1'],
			'MosTemp2' => $row['MosTemp2'],

			'CurrentInput' => $row['CurrentInput'],
			'CurrentInput2' => $row['CurrentInput2'],

 			'Heartbeat' => $row['Heartbeat'],

			'time'=> $row['time'],
			'id'=> $row['id']];
}
else {
	$table = null;
}
echo json_encode($table);
?>