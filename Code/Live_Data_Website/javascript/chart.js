/*
    Copyright (C) 2018  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat 
*/

google.charts.load('current', { 'packages': ['corechart'], 'language': 'pt_PT' });


function SOC_chart() {

    var mysqlData = $.ajax({
        url: "php/soc_chart.php",
        dataType: "JSON",
        async: false,
        data: {},
        success: function(x) {
            return x;
        }
    }).responseJSON;

    if (mysqlData == null) {
        alert("Não há dados a apresentar!");
        return;
    }
    console.log(mysqlData);

    var data = google.visualization.arrayToDataTable([
        [{ label: 'Time', type: 'datetime' }, { label: 'Percentage', type: 'number' }]
    ]);

    var phpDate, date;
    for (i = 0; i < Object.keys(mysqlData).length; i++) {
        phpDate = mysqlData[i]["time"].split(/[^0-9]/);
        date = new Date(phpDate[0], phpDate[1] - 1, phpDate[2], phpDate[3], phpDate[4], phpDate[5]);
        data.addRows([
            [date, parseInt(mysqlData[i]["SOC"])]
        ]);
    }

    var tempo = document.getElementById("janela").value;
    var startDate = new Date(date);
    startDate.setMinutes(date.getMinutes() - tempo);

    var dateFormatter = new google.visualization.DateFormat({
        pattern: "d MMMM yyyy, H:m:s"
    });
    dateFormatter.format(data, 0);

    var formatter = new google.visualization.NumberFormat({
        suffix: ' %',
        fractionDigits: 0
    });
    formatter.format(data, 1);

    var options = {
        title: 'SOC',
        legend: { position: 'none' },
        vAxis: { title: 'SOC (%)', viewWindow: { min: 0, max: 100 }, gridlines: { count: 10 }, ticks: [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100] },
        hAxis: { title: 'Time', format: 'H:m:s', viewWindow: { min: startDate, max: date } },
        pointSize: 5
    };

    var chart = new google.visualization.LineChart(document.getElementById('SOC_chart'));
    chart.draw(data, options);
}

function VOLTAGE_chart() {

    var mysqlData = $.ajax({
        url: "php/voltage_chart.php",
        dataType: "JSON",
        async: false,
        data: {},
        success: function(x) {
            return x;
        }
    }).responseJSON;

    if (mysqlData == null) {
        alert("Não há dados a apresentar!");
        return;
    }

    var data = google.visualization.arrayToDataTable([
        [{ label: 'Time', type: 'datetime' }, { label: 'Voltage', type: 'number' }]
    ]);

    var phpDate, date;
    for (i = 0; i < Object.keys(mysqlData).length; i++) {
        phpDate = mysqlData[i]["time"].split(/[^0-9]/);
        date = new Date(phpDate[0], phpDate[1] - 1, phpDate[2], phpDate[3], phpDate[4], phpDate[5]);
        data.addRows([
            [date, parseFloat(mysqlData[i]["Voltage"])]
        ]);
    }

    var tempo = document.getElementById("janela").value;
    var startDate = new Date(date);
    startDate.setMinutes(date.getMinutes() - tempo);

    var dateFormatter = new google.visualization.DateFormat({
        pattern: "d MMMM yyyy, H:m:s"
    });
    dateFormatter.format(data, 0);

    var formatter = new google.visualization.NumberFormat({
        suffix: ' V',
        fractionDigits: 2,
        decimalSymbol: ','
    });
    formatter.format(data, 1);

    var options = {
        title: 'Voltage',
        legend: { position: 'none' },
        vAxis: { title: 'Voltage (V)', viewWindow: { min: 30, max: 50 }, gridlines: { count: 11 }, ticks: [30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50] },
        hAxis: { title: 'Time', format: 'H:m:s', viewWindow: { min: startDate, max: date } },
        pointSize: 5
    };

    var chart = new google.visualization.LineChart(document.getElementById('VOLTAGE_chart'));
    chart.draw(data, options);
}

function RPM_chart() {

    var mysqlData = $.ajax({
        url: "php/RPM_chart.php",
        dataType: "JSON",
        async: false,
        data: {},
        success: function(x) {
            return x;
        }
    }).responseJSON;

    if (mysqlData == null) {
        alert("Não há dados a apresentar!");
        return;
    }

    var data = google.visualization.arrayToDataTable([
        [{ label: 'Time', type: 'datetime' }, { label: 'RPM', type: 'number' }]
    ]);

    var phpDate, date;
    for (i = 0; i < Object.keys(mysqlData).length; i++) {
        phpDate = mysqlData[i]["time"].split(/[^0-9]/);
        date = new Date(phpDate[0], phpDate[1] - 1, phpDate[2], phpDate[3], phpDate[4], phpDate[5]);
        data.addRows([
            [date, parseInt(mysqlData[i]["RPM"])]
        ]);
    }

    var tempo = document.getElementById("janela").value;
    var startDate = new Date(date);
    startDate.setMinutes(date.getMinutes() - tempo);

    var dateFormatter = new google.visualization.DateFormat({
        pattern: "d MMMM yyyy, H:m:s"
    });
    dateFormatter.format(data, 0);

    var formatter = new google.visualization.NumberFormat({
        suffix: ' RPM',
        fractionDigits: 0,
        groupingSymbol: ''
    });
    formatter.format(data, 1);

    var options = {
        title: 'RPM',
        legend: { position: 'none' },
        vAxis: { title: 'RPM', format: '', viewWindow: { min: 0, max: 4000 }, gridlines: { count: 8 }, ticks: [0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000] },
        hAxis: { title: 'Time', format: 'H:m:s', viewWindow: { min: startDate, max: date } },
        pointSize: 5
    };

    var chart = new google.visualization.LineChart(document.getElementById('RPM_chart'));
    chart.draw(data, options);
}

function Speed_chart() {

    var mysqlData = $.ajax({
        url: "php/Speed_chart.php",
        dataType: "JSON",
        async: false,
        data: {},
        success: function(x) {
            return x;
        }
    }).responseJSON;

    if (mysqlData == null) {
        alert("Não há dados a apresentar!");
        return;
    }

    var data = google.visualization.arrayToDataTable([
        [{ label: 'Time', type: 'datetime' }, { label: 'Speed', type: 'number' }]
    ]);

    var phpDate, date;
    for (i = 0; i < Object.keys(mysqlData).length; i++) {
        phpDate = mysqlData[i]["time"].split(/[^0-9]/);
        date = new Date(phpDate[0], phpDate[1] - 1, phpDate[2], phpDate[3], phpDate[4], phpDate[5]);
        data.addRows([
            [date, parseFloat(mysqlData[i]["Speed"])]
        ]);
    }

    var tempo = document.getElementById("janela").value;
    var startDate = new Date(date);
    startDate.setMinutes(date.getMinutes() - tempo);

    var dateFormatter = new google.visualization.DateFormat({
        pattern: "d MMMM yyyy, H:m:s"
    });
    dateFormatter.format(data, 0);

    var formatter = new google.visualization.NumberFormat({
        suffix: ' kN',
        fractionDigits: 1,
        groupingSymbol: '',
        decimalSymbol: ','
    });
    formatter.format(data, 1);

    var options = {
        title: 'Speed',
        legend: { position: 'none' },
        vAxis: { title: 'Speed (Knots)', format: '', viewWindow: { min: 0, max: 30 }, gridlines: { count: 6 }, ticks: [0, 5, 10, 15, 20, 25, 30] },
        hAxis: { title: 'Time', format: 'H:m:s', viewWindow: { min: startDate, max: date } },
        pointSize: 5
    };

    var chart = new google.visualization.LineChart(document.getElementById('Speed_chart'));
    chart.draw(data, options);
}

function T_Mot_chart() {

    var mysqlData = $.ajax({
        url: "php/T_Mot_chart.php",
        dataType: "JSON",
        async: false,
        data: {},
        success: function(x) {
            return x;
        }
    }).responseJSON;
    if (mysqlData == null) {
        alert("Não há dados a apresentar!");
        return;
    }
    console.log(mysqlData);

    var data = google.visualization.arrayToDataTable([
        [{ label: 'Time', type: 'datetime' }, { label: 'T Mot', type: 'number' }]
    ]);

    var phpDate, date;
    for (i = 0; i < Object.keys(mysqlData).length; i++) {
        phpDate = mysqlData[i]["time"].split(/[^0-9]/);
        date = new Date(phpDate[0], phpDate[1] - 1, phpDate[2], phpDate[3], phpDate[4], phpDate[5]);
        data.addRows([
            [date, parseFloat(mysqlData[i]["T_Mot"])]
        ]);
    }

    var tempo = document.getElementById("janela").value;
    var startDate = new Date(date);
    startDate.setMinutes(date.getMinutes() - tempo);

    var dateFormatter = new google.visualization.DateFormat({
        pattern: "d MMMM yyyy, H:m:s"
    });
    dateFormatter.format(data, 0);

    var formatter = new google.visualization.NumberFormat({
        suffix: ' ºC',
        fractionDigits: 2,
        groupingSymbol: '',
        decimalSymbol: ','
    });
    formatter.format(data, 1);

    var options = {
        title: 'Temperatura do Motor',
        legend: { position: 'none' },
        vAxis: { title: 'T Mot (ºC)', format: '', viewWindow: { min: 0, max: 50 }, gridlines: { count: 10 }, ticks: [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50] },
        hAxis: { title: 'Time', format: 'H:m:s', viewWindow: { min: startDate, max: date } },
        pointSize: 5
    };

    var chart = new google.visualization.LineChart(document.getElementById('T_Mot_chart'));
    chart.draw(data, options);
}

function T_Bat_chart() {

    var mysqlData = $.ajax({
        url: "php/T_Bat_chart.php",
        dataType: "JSON",
        async: false,
        data: {},
        success: function(x) {
            return x;
        }
    }).responseJSON;

    if (mysqlData == null) {
        alert("Não há dados a apresentar!");
        return;
    }

    var data = google.visualization.arrayToDataTable([
        [{ label: 'Time', type: 'datetime' }, { label: 'T Bat', type: 'number' }]
    ]);

    var phpDate, date;
    for (i = 0; i < Object.keys(mysqlData).length; i++) {
        phpDate = mysqlData[i]["time"].split(/[^0-9]/);
        date = new Date(phpDate[0], phpDate[1] - 1, phpDate[2], phpDate[3], phpDate[4], phpDate[5]);
        data.addRows([
            [date, parseFloat(mysqlData[i]["T_Bat"])]
        ]);
    }

    var tempo = document.getElementById("janela").value;
    var startDate = new Date(date);
    startDate.setMinutes(date.getMinutes() - tempo);

    var dateFormatter = new google.visualization.DateFormat({
        pattern: "d MMMM yyyy, H:m:s"
    });
    dateFormatter.format(data, 0);

    var formatter = new google.visualization.NumberFormat({
        suffix: ' ºC',
        fractionDigits: 2,
        groupingSymbol: '',
        decimalSymbol: ','
    });
    formatter.format(data, 1);

    var options = {
        title: 'Temperatura da Bateria',
        legend: { position: 'none' },
        vAxis: { title: 'T Bat (ºC)', format: '', viewWindow: { min: 0, max: 50 }, gridlines: { count: 10 }, ticks: [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50] },
        hAxis: { title: 'Time', format: 'H:m:s', viewWindow: { min: startDate, max: date } },
        pointSize: 5
    };

    var chart = new google.visualization.LineChart(document.getElementById('T_Bat_chart'));
    chart.draw(data, options);
}

function T_BMS_chart() {

    var mysqlData = $.ajax({
        url: "php/T_BMS_chart.php",
        dataType: "JSON",
        async: false,
        data: {},
        success: function(x) {
            return x;
        }
    }).responseJSON;

    if (mysqlData == null) {
        alert("Não há dados a apresentar!");
        return;
    }

    var data = google.visualization.arrayToDataTable([
        [{ label: 'Time', type: 'datetime' }, { label: 'T BMS', type: 'number' }]
    ]);

    var phpDate, date;
    for (i = 0; i < Object.keys(mysqlData).length; i++) {
        phpDate = mysqlData[i]["time"].split(/[^0-9]/);
        date = new Date(phpDate[0], phpDate[1] - 1, phpDate[2], phpDate[3], phpDate[4], phpDate[5]);
        data.addRows([
            [date, parseFloat(mysqlData[i]["T_BMS"])]
        ]);
    }

    var tempo = document.getElementById("janela").value;
    var startDate = new Date(date);
    startDate.setMinutes(date.getMinutes() - tempo);

    var dateFormatter = new google.visualization.DateFormat({
        pattern: "d MMMM yyyy, H:m:s"
    });
    dateFormatter.format(data, 0);

    var formatter = new google.visualization.NumberFormat({
        suffix: ' ºC',
        fractionDigits: 2,
        groupingSymbol: '',
        decimalSymbol: ','
    });
    formatter.format(data, 1);

    var options = {
        title: 'Temperatura do BMS',
        legend: { position: 'none' },
        vAxis: { title: 'T BMS (ºC)', format: '', viewWindow: { min: 0, max: 50 }, gridlines: { count: 10 }, ticks: [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50] },
        hAxis: { title: 'Time', format: 'H:m:s', viewWindow: { min: startDate, max: date } },
        pointSize: 5
    };

    var chart = new google.visualization.LineChart(document.getElementById('T_BMS_chart'));
    chart.draw(data, options);
}

function T_TD_chart() {

    var mysqlData = $.ajax({
        url: "php/T_TD_chart.php",
        dataType: "JSON",
        async: false,
        data: {},
        success: function(x) {
            return x;
        }
    }).responseJSON;

    if (mysqlData == null) {
        alert("Não há dados a apresentar!");
        return;
    }

    var data = google.visualization.arrayToDataTable([
        [{ label: 'Time', type: 'datetime' }, { label: 'T TD', type: 'number' }]
    ]);

    var phpDate, date;
    for (i = 0; i < Object.keys(mysqlData).length; i++) {
        phpDate = mysqlData[i]["time"].split(/[^0-9]/);
        date = new Date(phpDate[0], phpDate[1] - 1, phpDate[2], phpDate[3], phpDate[4], phpDate[5]);
        data.addRows([
            [date, parseFloat(mysqlData[i]["T_TD"])]
        ]);
    }

    var tempo = document.getElementById("janela").value;
    var startDate = new Date(date);
    startDate.setMinutes(date.getMinutes() - tempo);

    var dateFormatter = new google.visualization.DateFormat({
        pattern: "d MMMM yyyy, H:m:s"
    });
    dateFormatter.format(data, 0);

    var formatter = new google.visualization.NumberFormat({
        suffix: ' ºC',
        fractionDigits: 2,
        groupingSymbol: '',
        decimalSymbol: ','
    });
    formatter.format(data, 1);

    var options = {
        title: 'Temperatura da caixa de TD',
        legend: { position: 'none' },
        vAxis: { title: 'T TD (ºC)', format: '', viewWindow: { min: 0, max: 50 }, gridlines: { count: 10 }, ticks: [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50] },
        hAxis: { title: 'Time', format: 'H:m:s', viewWindow: { min: startDate, max: date } },
        pointSize: 5
    };

    var chart = new google.visualization.LineChart(document.getElementById('T_TD_chart'));
    chart.draw(data, options);
}

function Motor_chart() {

    var mysqlData = $.ajax({
        url: "php/Motor_chart.php",
        dataType: "JSON",
        async: false,
        data: {},
        success: function(x) {
            return x;
        }
    }).responseJSON;

    if (mysqlData == null) {
        alert("Não há dados a apresentar!");
        return;
    }

    var data = google.visualization.arrayToDataTable([
        [{ label: 'Time', type: 'datetime' }, { label: 'I Motor', type: 'number' }]
    ]);

    var phpDate, date;
    for (i = 0; i < Object.keys(mysqlData).length; i++) {
        phpDate = mysqlData[i]["time"].split(/[^0-9]/);
        date = new Date(phpDate[0], phpDate[1] - 1, phpDate[2], phpDate[3], phpDate[4], phpDate[5]);
        data.addRows([
            [date, parseFloat(mysqlData[i]["Motor"])]
        ]);
    }

    var tempo = document.getElementById("janela").value;
    var startDate = new Date(date);
    startDate.setMinutes(date.getMinutes() - tempo);

    var dateFormatter = new google.visualization.DateFormat({
        pattern: "d MMMM yyyy, H:m:s"
    });
    dateFormatter.format(data, 0);

    var formatter = new google.visualization.NumberFormat({
        suffix: ' A',
        fractionDigits: 2,
        groupingSymbol: '',
        decimalSymbol: ','
    });
    formatter.format(data, 1);

    var options = {
        title: 'Corrente do Motor',
        legend: { position: 'none' },
        vAxis: { title: 'I Motor (A)', format: '', viewWindow: { min: 0, max: 300 }, gridlines: { count: 6 }, ticks: [0, 50, 100, 150, 200, 250, 300] },
        hAxis: { title: 'Time', format: 'H:m:s', viewWindow: { min: startDate, max: date } },
        pointSize: 5
    };

    var chart = new google.visualization.LineChart(document.getElementById('Motor_chart'));
    chart.draw(data, options);
}

function Paineis_chart() {

    var mysqlData = $.ajax({
        url: "php/Paineis_chart.php",
        dataType: "JSON",
        async: false,
        data: {},
        success: function(x) {
            return x;
        }
    }).responseJSON;

    if (mysqlData == null) {
        alert("Não há dados a apresentar!");
        return;
    }

    var data = google.visualization.arrayToDataTable([
        [{ label: 'Time', type: 'datetime' }, { label: 'I Paineis', type: 'number' }]
    ]);

    var phpDate, date;
    for (i = 0; i < Object.keys(mysqlData).length; i++) {
        phpDate = mysqlData[i]["time"].split(/[^0-9]/);
        date = new Date(phpDate[0], phpDate[1] - 1, phpDate[2], phpDate[3], phpDate[4], phpDate[5]);
        data.addRows([
            [date, parseFloat(mysqlData[i]["Paineis"])]
        ]);
    }

    var tempo = document.getElementById("janela").value;
    var startDate = new Date(date);
    startDate.setMinutes(date.getMinutes() - tempo);

    var dateFormatter = new google.visualization.DateFormat({
        pattern: "d MMMM yyyy, H:m:s"
    });
    dateFormatter.format(data, 0);

    var formatter = new google.visualization.NumberFormat({
        suffix: ' A',
        fractionDigits: 2,
        groupingSymbol: '',
        decimalSymbol: ','
    });
    formatter.format(data, 1);

    var options = {
        title: 'Corrente dos Paineis',
        legend: { position: 'none' },
        vAxis: { title: 'I Paineis (A)', format: '', viewWindow: { min: 0, max: 50 }, gridlines: { count: 5 }, ticks: [0, 10, 20, 30, 40, 50] },
        hAxis: { title: 'Time', format: 'H:m:s', viewWindow: { min: startDate, max: date } },
        pointSize: 5
    };

    var chart = new google.visualization.LineChart(document.getElementById('Paineis_chart'));
    chart.draw(data, options);
}

function BAT_chart() {

    var mysqlData = $.ajax({
        url: "php/BAT_chart.php",
        dataType: "JSON",
        async: false,
        data: {},
        success: function(x) {
            return x;
        }
    }).responseJSON;

    if (mysqlData == null) {
        alert("Não há dados a apresentar!");
        return;
    }

    var data = google.visualization.arrayToDataTable([
        [{ label: 'Time', type: 'datetime' }, { label: 'I Bateria', type: 'number' }]
    ]);

    var phpDate, date;
    for (i = 0; i < Object.keys(mysqlData).length; i++) {
        phpDate = mysqlData[i]["time"].split(/[^0-9]/);
        date = new Date(phpDate[0], phpDate[1] - 1, phpDate[2], phpDate[3], phpDate[4], phpDate[5]);
        data.addRows([
            [date, parseFloat(mysqlData[i]["BAT"])]
        ]);
    }

    var tempo = document.getElementById("janela").value;
    var startDate = new Date(date);
    startDate.setMinutes(date.getMinutes() - tempo);

    var dateFormatter = new google.visualization.DateFormat({
        pattern: "d MMMM yyyy, H:m:s"
    });
    dateFormatter.format(data, 0);

    var formatter = new google.visualization.NumberFormat({
        suffix: ' A',
        fractionDigits: 2,
        groupingSymbol: '',
        decimalSymbol: ','
    });
    formatter.format(data, 1);

    var options = {
        title: 'Corrente da Bateria',
        legend: { position: 'none' },
        vAxis: { title: 'I Bateria (A)', format: '', viewWindow: { min: -300, max: 100 }, gridlines: { count: 4 }, ticks: [-300, -200, -100, 0, 100] },
        hAxis: { title: 'Time', format: 'H:m:s', viewWindow: { min: startDate, max: date } },
        pointSize: 5
    };

    var chart = new google.visualization.LineChart(document.getElementById('BAT_chart'));
    chart.draw(data, options);
}