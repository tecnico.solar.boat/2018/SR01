/*
    Copyright (C) 2018  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat 
*/

google.charts.load('current', { 'packages': ['gauge'] });
google.charts.setOnLoadCallback(SOC);
google.charts.setOnLoadCallback(VOLTAGE);
google.charts.setOnLoadCallback(SPEED);
google.charts.setOnLoadCallback(IBAT);
google.charts.setOnLoadCallback(I_M0);
google.charts.setOnLoadCallback(T_Mot0);
google.charts.setOnLoadCallback(I_M2);
google.charts.setOnLoadCallback(T_Mot2);


var mysqlDataFoils;
var mysqlDataMotores;
setInterval(function() {
    $.ajax({
        url: "php/Foils.php",
        dataType: "JSON",
        data: {},
        success: function(x) {
            mysqlDataFoils = x;
        }
    });
    $.ajax({
        url: "php/Motores.php",
        dataType: "JSON",
        data: {},
        success: function(x) {
            mysqlDataMotores = x;
        }
    });
    $.ajax({
        url: "php/BSS.php",
        dataType: "JSON",
        data: {},
        success: function(x) {
            mysqlDataBMS = x;
        }
    });
}, 300);

function SOC() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['SOC', -500]
    ]);

    var options = {
        width: 1000,
        height: 200,
        redFrom: 0,
        redTo: 10,
        yellowFrom: 10,
        yellowTo: 25,
        minorTicks: 5,
        majorTicks: ['0', '25', '50', '75', '100']
    };

    var formatter = new google.visualization.NumberFormat({
        suffix: '%',
        fractionDigits: 0
    });

    formatter.format(data, 1);

    var chart = new google.visualization.Gauge(document.getElementById('SOC'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, mysqlDataBMS["SOC"]);
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 300);

}

function VOLTAGE() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['Voltage', -500]
    ]);
    var options = {
        width: 1000,
        height: 200,
        min: 35,
        max: 51,
        redFrom: 50.4, redTo: 51,
        yellowFrom: 35, yellowTo: 36, yellowColor: '#DC3912',
        
        minorTicks: 4,
        majorTicks: ['35', '39', '43', '47', '51']
    };
    var formatter = new google.visualization.NumberFormat({
        suffix: 'V',
        fractionDigits: 2,
        decimalSymbol: ','
    });
    formatter.format(data, 1);
    var chart = new google.visualization.Gauge(document.getElementById('VOLTAGE'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, mysqlDataBMS["Voltage"]);
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 300);
}


function SPEED() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['Speed', -500]
    ]);
    var options = {
        width: 1000,
        height: 200,
        min: 0,
        max: 60,
        redFrom: 55,
        redTo: 60,
        yellowFrom: 45,
        yellowTo: 55,
        minorTicks: 5,
        majorTicks: ['0', '10', '20', '30', '40', '50', '60']
    };
    var formatter = new google.visualization.NumberFormat({
        suffix: 'km/h',
        fractionDigits: 1,
        decimalSymbol: ','
    });
    formatter.format(data, 1);
    var chart = new google.visualization.Gauge(document.getElementById('SPEED'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, (Math.sqrt(Math.pow(mysqlDataFoils["velX"],2)+Math.pow(mysqlDataFoils["velY"],2))*3.6));
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 300);
}

function IBAT() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['I BAT', -500]
    ]);
    var options = {
        width: 1000, height: 200,
        min: -350, max: 50,
        redFrom: -350, redTo: -300,
        yellowFrom: -300, yellowTo: -200,
        greenFrom: 0, greenTo: 50,
        minorTicks: 10,
        majorTicks: ['-350', '-250', '-150', '-50', '50']
    };
    var formatter = new google.visualization.NumberFormat({
        suffix: 'A',
        fractionDigits: 1,
        decimalSymbol: ','
    });
    formatter.format(data, 1);
    var chart = new google.visualization.Gauge(document.getElementById('IBAT'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, mysqlDataBMS["Current"]);
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 300);
}

function I_M0() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['I M1', -500]
    ]);
    var options = {
        width: 1000, height: 200,
        min: 0, max: 175,
        redFrom: 160, redTo: 175,
        //yellowFrom: -300, yellowTo: -200,
        //greenFrom: 0, greenTo: 50,
        minorTicks: 5,
        majorTicks: ['0', '25', '50', '75', '100', '125', '150', '175' ]
    };
    var formatter = new google.visualization.NumberFormat({
        suffix: 'A',
        fractionDigits: 1,
        decimalSymbol: ','
    });
    formatter.format(data, 1);
    var chart = new google.visualization.Gauge(document.getElementById('I_M0'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, mysqlDataMotores["CurrentInput"]);
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 300);
}

function T_Mot0() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['T M1', -500]
    ]);
    var options = {
        width: 1000,
        height: 200,
        min: 0,
        max: 150,
        redFrom: 80,
        redTo: 150,
        yellowFrom: 50,
        yellowTo: 80,
        minorTicks: 5,
        majorTicks: ['0', '50', '100', '150']
    };
    var formatter = new google.visualization.NumberFormat({
        suffix: ' ºC',
		fontsize: 7,
        fractionDigits: 0,
        decimalSymbol: ',',
        groupingSymbol: '.',
    });
    formatter.format(data, 1);
    var chart = new google.visualization.Gauge(document.getElementById('T_Mot0'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, parseInt(mysqlDataMotores["MosTemp1"]));
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 300);
}


function I_M2() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['I M2', -500]
    ]);
    var options = {
        width: 1000, height: 200,
        min: 0, max: 175,
        redFrom: 160, redTo: 175,
        //yellowFrom: -300, yellowTo: -200,
        //greenFrom: 0, greenTo: 50,
        minorTicks: 5,
        majorTicks: ['0', '25', '50', '75', '100', '125', '150', '175' ]
    };
    var formatter = new google.visualization.NumberFormat({
        suffix: 'A',
        fractionDigits: 1,
        decimalSymbol: ','
    });
    formatter.format(data, 1);
    var chart = new google.visualization.Gauge(document.getElementById('I_M2'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, mysqlDataMotores["CurrentInput2"]);
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 300);
}

function T_Mot2() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['T M2', -500]
    ]);
    var options = {
        width: 1000,
        height: 200,
        min: 0,
        max: 150,
        redFrom: 80,
        redTo: 150,
        yellowFrom: 50,
        yellowTo: 80,
        minorTicks: 5,
        majorTicks: ['0', '50', '100', '150']
    };
    var formatter = new google.visualization.NumberFormat({
        suffix: ' ºC',
		fontsize: 7,
        fractionDigits: 0,
        decimalSymbol: ',',
        groupingSymbol: '.',
    });
    formatter.format(data, 1);
    var chart = new google.visualization.Gauge(document.getElementById('T_Mot2'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, parseInt(mysqlDataMotores["MosTemp2"]));
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 300);
}


