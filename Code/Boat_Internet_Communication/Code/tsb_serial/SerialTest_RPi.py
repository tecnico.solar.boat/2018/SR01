'''
    Copyright (C) 2018  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat  
'''



from serial import Serial
import time

# This file serves the purpose of testing the Teensy tsb_serial API
# that implements the communication between a Teensy and a Raspberry Pi

txbuf1 = [172, 4, 3, 0, 1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121, 27, 173] 	# Valid message
txbuf2 = [172, 4, 3, 0, 1, 4, 9, 16, 25, 36, 49, 64, 80, 100, 121, 27, 173]		# Invalid message

ser = Serial('/dev/ttyACM1')
x=1

while True: 
	if x==1:
		x=2	
		ser.write(txbuf1)
	else:
		x=1
		ser.write(txbuf2)

	c = ser.inWaiting()
	if c > 0:
		data = ser.read_all()
		print(data)

	time.sleep(1)