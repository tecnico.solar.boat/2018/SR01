'''
    Copyright (C) 2018  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat  
'''


import os
import sys
import serial
from threading import Thread, Lock
import time
import datetime
from multiprocessing.connection import Listener
from multiprocessing.connection import Client


def send_fifo_lcd(buffer, dataid):
    print("Sending to socket", dataid)

    address = ('localhost', (6000+dataid))

    conn = Client(address, authkey=b'\x00')
    conn.send(buffer)
    conn.close()

    #  print("Error sending")
       # print("Cant connect to lcd")
while True:
    message="ola"
    dataid=2
    send_fifo_lcd(message,dataid)
    time.sleep(1)
