'''
    Copyright (C) 2018  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat 
'''

# Variables for packing LCD data
# Has a similar effect as global variables in C
# The default value is -1

#Motor variables for the LCD
asked_torque = -1
motor_ready_M0 = -1
motor_ready_M1 = -1
fail_m0=-1
fail_m1=-1
temp_mos=-1
rpm=-1
power_input_motor=-1

#Foils variabls for the LCD
velX=-1
velY=-1


#BMS Variables for the LCD
bat_voltage = -1
bat_power=-1
solar_power=-1
warnings = ""
status=-1

#Timer
timer_interval_send=0

#LCD variables
motor_running = bss_running = foils_running =0
waiting_lcd=0