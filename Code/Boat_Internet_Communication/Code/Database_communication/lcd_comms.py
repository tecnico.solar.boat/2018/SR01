'''
    Copyright (C) 2018  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat 
'''

import os
import sys
import serial
from multiprocessing import Process
import time
from threading import Lock
import datetime
from concurrent.futures import ThreadPoolExecutor
from multiprocessing.connection import Listener
import lcd

lcd_on = os.system("ls /dev/lcd")
mutex = Lock()
first_time=0

if lcd_on == 0:
    screen = serial.Serial("/dev/lcd", baudrate=115200, timeout=1.0)

try:
    address = ('localhost', 6002)
    os.unlink(address)

    address = ('localhost', 6003)
    os.unlink(address)

    address = ('localhost', 6004)
    os.unlink(address)
except:
    print("Already Unliked")


def time_msg():
    """Composes and sends the time message

    Convert the current time into a string of 6 bytes, according to the order in the table of the wiki
    and sends it to each teensy to perform its time update

    Args:
        None

    Returns:

        0 if the function succeed
        -1 if the function fail

        For further information consult the wiki

    """

    time_msg_size = 6 + 5
    time_m = b'\xac\x00\x01'
    current_t = datetime.datetime.now()
    year = current_t.year - 2000
    time_m += current_t.hour.to_bytes(1, 'big')
    time_m += current_t.minute.to_bytes(1, 'big')
    time_m += current_t.second.to_bytes(1, 'big')
    time_m += current_t.day.to_bytes(1, 'big')
    time_m += current_t.month.to_bytes(1, 'big')
    time_m += year.to_bytes(1, 'big')

    checksum = 0

    for i in range(1, time_msg_size - 2):
        checksum ^= time_m[i]

    time_m += checksum.to_bytes(1, 'big')
    time_m += b'\xad'

    #port.write(time_m)
    if lcd_on == 0:
        screen.write(time_m)
    print("Time sent")
    return 0
def lcd_motor_msg():
    print("Wait on motor")
    lcd.motor_running=1
    address = ('localhost', 6002)  # family is deduced to be 'AF_INET'
    listener = Listener(address, authkey=b'\x00')
    conn = listener.accept()
    msg = conn.recv()
    if lcd_on==0:
        mutex.acquire()
        print("Sending ", msg)
        screen.write(msg)
        mutex.release()
    else:
        print(msg)
    conn.close()

    listener.close()
    lcd.motor_running=0
    print("motor recv")


def lcd_foils_msg():
    print("Wait on foils")
    lcd.foils_running=1
    address = ('localhost', 6003)  # family is deduced to be 'AF_INET'
    listener = Listener(address, authkey=b'\x00')
    conn = listener.accept()
    msg = conn.recv()
    if lcd_on==0:
        mutex.acquire()
        print("Sending ", msg)
        screen.write(msg)
        mutex.release()
    else:
        print(msg)
    conn.close()

    listener.close()
    lcd.foils_running=0
    print("foils rcv")


def lcd_bss_msg():
    print("Wait on bss")
    lcd.bss_running=1
    address = ('localhost', 6004)  # family is deduced to be 'AF_INET'

    listener = Listener(address, authkey=b'\x00')
    conn = listener.accept()
    msg = conn.recv()
    if lcd_on==0:
        mutex.acquire()
        print("Sending ", msg)
        screen.write(msg)
        mutex.release()
    else:
        print(msg)
    conn.close()

    listener.close()
    lcd.bss_running=0
    print("bss recv")

while True:
    # try:
    #     if first_time ==0:
    #         time_msg()
    #         first_time=1
    #     executor = ThreadPoolExecutor(max_workers=3)
    #     #thread_bss = executor.submit(lcd_bss_msg)
    #     #thread_foils = executor.submit(lcd_foils_msg)
    #     thread_motor = executor.submit(lcd_motor_msg)
    #     executor.wait(thread_motor)
    #     #thread_bss.start()
    #     #thread_foils.start()
    #     #thread_motor.start()
    #     #thread_motor.join()
    #     #thread_foils.join()
    #     #thread_bss.join()
    # except:
    #     if lcd_on == 0:
    #         screen.close()
    #         sys.exit(-1)
    # #thread_motor = Thread(target=lcd_motor_msg())
    # #thread_motor.start()
    try:

        if first_time == 0:


            time_msg()
            first_time = 1
            executor = ThreadPoolExecutor(max_workers=3)
            thread_motor = executor.submit(lcd_motor_msg)
            lcd.bss_running=1
            thread_foils = executor.submit(lcd_foils_msg)
            lcd.motor_running=1
            thread_bss = executor.submit(lcd_bss_msg)
            lcd.foils_running=1

        # thread_bss = executor.submit(lcd_bss_msg)
        # thread_foils = executor.submit(lcd_foils_msg)
        if lcd.motor_running==0:
            thread_motor = executor.submit(lcd_motor_msg)
            lcd.motor_running=1
        if lcd.foils_running==0:
            thread_foils = executor.submit(lcd_foils_msg)
            lcd.foils_running=1
        if lcd.bss_running==0:
            thread_bss = executor.submit(lcd_bss_msg)
            lcd.bss_running=1
        #time.sleep(1)
    except:
        print("Failed")
        if lcd_on==0:
            screen.close()
        sys.exit(-1)

