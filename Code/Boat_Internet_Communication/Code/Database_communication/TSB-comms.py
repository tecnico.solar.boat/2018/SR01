'''
    Copyright (C) 2018  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat 
'''

# coding=utf-8
import serial, time, pymysql, struct, datetime
import numpy as np
import tsb, lcd
import sys
import os
import socket
import time

from timeit import default_timer as timer
from multiprocessing.connection import Client
from threading import Timer


def internet(host="8.8.8.8", port=53, timeout=3):
    """Check Internet connection

    Tries to ping google every second to ensure that there is an internet connection
    before starting to execute the rest of the code

    Args:
        host: Google ping
        port: Port to be pinged
        timeout: Timeout

    Returns:

        0 if the function succeeds

     For further information on how each message is handled consult the wiki at
     https://gitlab.com/tecnicosb/tsb-es/boatcomm/wikis/TSB-Protocol
    """
    try:
        socket.setdefaulttimeout(timeout)
        socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect((host, port))
        return True
    except Exception as ex:
        return False


while not internet():
    print("Waiting for connection");
    time.sleep(0.5)

##database connection##
DB_NAME = 'INSERT YOUR DATA HERE'
DB_HOST = 'INSERT YOUR DATA HERE'
DB_USER = 'INSERT YOUR DATA HERE'
DB_PASS = 'INSERT YOUR DATA HERE'

conn = pymysql.connect(DB_HOST, DB_USER, DB_PASS, DB_NAME, autocommit=True)
cur = conn.cursor()
#######################

##Select source from the console arguments
if len(sys.argv) < 2:
    sys.exit("No arguments, exiting")

if sys.argv[1] == 'motor':
    print("Motor teensy communication")
    x = os.system("ls /dev/motor")
    if x == 0:
        port = serial.Serial("/dev/motor", baudrate=115200, timeout=1.0)
    else:
        sys.exit("Motor controller not connected")
elif sys.argv[1] == 'foils':
    print("Foil teensy communication")
    x = os.system("ls /dev/foils")
    if x == 0:
        port = serial.Serial("/dev/foils", baudrate=115200, timeout=1.0)
    else:
        sys.exit("Foils controller not connected")
elif sys.argv[1] == 'bss':
    print("BSS teensy communication")
    x = os.system("ls /dev/bss")
    if x == 0:
        port = serial.Serial("/dev/bss", baudrate=115200, timeout=1.0)
    else:
        sys.exit("BSS not connected")
else:
    sys.exit("Unknown argument, exiting")

lcd_on = os.system("ls /dev/lcd")

now = datetime.datetime.now()


def send_fifo_lcd(buffer, dataid):
    """Performs the necessary socket operations

    Performs the necessary socket operations to communicate with the lcd process.

    Args:
        buf: The message returned from the receive_msg() function
        dataid: Message ID

    Returns:

        0 if the function succeeds

     For further information on how each message is handled consult the wiki at
     https://gitlab.com/tecnicosb/tsb-es/boatcomm/wikis/TSB-Protocol
    """
    print("Sending to socket", dataid)

    address = ('localhost', (6000+dataid))
    try:
        conn = Client(address, authkey=b'\x00')
        conn.send(buffer)
        conn.close()
    except:
        print("Error sending")

    lcd.waiting_lcd=0

def receive_msg():
    """Receives a message from the serial

    Reads and concatenates the bytes written on the serial by the teensy
    and performs a checksum to certify message integrity

    Args:
        None

    Returns:

        The message received as a string of bytes in the following fashion

        b'\x00\x00\x00\x00\x00\x00\x00\x00\x00'

        The meaning of the bytes received is exposed in the repositories wiki
        https://gitlab.com/tecnicosb/tsb-es/boatcomm/wikis/TSB-Protocol

    """
    # print("goin read")
    receive = False
    msg_size = 0
    checksum = 0
    uart_buf = b'\x00'
    while port.isOpen:

        if port.in_waiting:
            incoming = port.read().hex()

            if incoming != "":
                aux = int(incoming, 16)
                aux = bytes([aux])
                incoming = aux

            if incoming == b'\xac' and receive == False:
                msg_size += 1
                receive = True
                uart_buf = incoming

            elif incoming == b'\xad':
                msg_size += 1
                receive = False
                uart_buf += incoming
                break
            elif receive:
                uart_buf += incoming
                msg_size += 1

    for i in range(1, msg_size - 2):
        checksum ^= int(uart_buf[i])

    if checksum == uart_buf[msg_size - 2]:
        print("Message received ", uart_buf)
        return uart_buf

    else:
        print("CHecksum received failed")
        return b'\00\00\00\00\00\00\00'

    # return uart_buf


def deal_with_message(buf):
    """Act according to the message received

    Check the important information contained on the message and executes the
    code specific for each message, according to the tables defined on the project wiki

    Args:
        buf: The message returned from the receive_msg() function

    Returns:

        0 if the function succeeds

     For further information on how each message is handled consult the wiki at
     https://gitlab.com/tecnicosb/tsb-es/boatcomm/wikis/TSB-Protocol
    """
    tsb.Received_addr = buf[1]

    # Motor controller code
    if buf[1] == 1:
        decode_motor_msg(buf)

    # Foil Controller Code
    if buf[1] == 2:
        decode_foils_msg(buf)

    # BSS code
    if buf[1] == 3:
        print("Message From BSS")
        decode_bss_msg(buf)

    # print("Message dealt with")
    return 0


def time_msg():
    """Composes and sends the time message

    Convert the current time into a string of 6 bytes, according to the order in the table of the wiki
    and sends it to each teensy to perform its time update

    Args:
        None

    Returns:

        0 if the function succeed
        -1 if the function fail

        For further information consult the wiki

    """

    time_msg_size = 6 + 5
    time_m = b'\xac\x00\x01'
    current_t = datetime.datetime.now()
    year = current_t.year - 2000
    time_m += current_t.hour.to_bytes(1, 'big')
    time_m += current_t.minute.to_bytes(1, 'big')
    time_m += current_t.second.to_bytes(1, 'big')
    time_m += current_t.day.to_bytes(1, 'big')
    time_m += current_t.month.to_bytes(1, 'big')
    time_m += year.to_bytes(1, 'big')

    checksum = 0

    for i in range(1, time_msg_size - 2):
        checksum ^= time_m[i]

    time_m += checksum.to_bytes(1, 'big')
    time_m += b'\xad'

    port.write(time_m)
    #if lcd_on == 0:
    #    screen.write(time_m)
    print("Time sent")
    return 0


def lcd_msg(data, data_id): #Hanging the code after a few messages
    """Composes and sends the lcd message

    Receive the data to be sent to the lcd for this process, ergo this teensy, pack it and
    ship it to the lcd teensy to be handled and displayed. Every piece of data sent as int16_t,
    the handling and scaling will be done by the lcd teensy.

    Args:
        data: The data tuple to be packed and sent
        data_id: A data data_id, specific for every message, in order to differentiate them


    Returns:
        0 if the function succeed
        -1 if the function fail


    For further information consult the wiki at
    https://gitlab.com/tecnicosb/tsb-es/boatcomm/wikis/TSB-Protocol
    """

    lcd_m = b'\xac\x00'
    lcd_m += data_id.to_bytes(1, 'big')

    # Motor variables
    if data_id == 2:
        data_auxiliar = [struct.pack("!f", i) for i in data]
        print(data)
        print(data_auxiliar)
        lcd_m += data_auxiliar[0]
        lcd_m += data_auxiliar[1]
        lcd_m += data_auxiliar[2]


    # Foils variables
    if data_id == 3:
        data_auxiliar = [int(i) for i in data]
        for i in range(0, 2):
            lcd_m += data[i]

    # BSS variables

    #lcd_table = (lcd.bat_voltage, lcd.bat_power, lcd.solar_power, lcd.status, lcd.warnings)
    if data_id == 4:
        #data_auxiliar = [struct.pack("!f",i) for i in data]
        #if data_auxiliar[0] < 0:
         #   data_auxiliar[0] *= -1
        data_auxiliar = [struct.pack("!f", i) for i in data]
        print(data)
        print(data_auxiliar)
        lcd_m += data_auxiliar[0]
        lcd_m += data_auxiliar[1]
        lcd_m += data_auxiliar[2]
        lcd_m += data_auxiliar[3]

        
#        for i in range(0, 3):
            #if data_auxiliar[i] < 0:
            #    data_auxiliar[i] *= -1
  #          lcd_m += data_auxiliar[i]
 #       for i in range(3, 5):
            #if data_auxiliar[i] < 0:
            #    data_auxiliar[i] *= -1
   #         lcd_m += data_auxiliar[i]

    checksum = 0
    for i in range(1, len(lcd_m)):
        checksum ^= lcd_m[i]

    lcd_m += checksum.to_bytes(1, 'big')
    lcd_m += b'\xad'
    #print("Sending to fifo",lcd_m)
    if lcd.waiting_lcd ==0:
        aux= (lcd_m,data_id)
        t=Timer(0.5,send_fifo_lcd,aux)
        lcd.waiting_lcd=1
        t.start()

    return 0


def decode_motor_msg(motor_buffer):
    """Act according to the message received

    Check the important information contained on the message and executes the
    code specific for each message, according to the tables defined on the project wiki
    for the Motor Controller
    Args:
        buf: The message returned from the receive_msg() function

    Returns:

        0 if the function succeeds

     For further information on how each message is handled consult the wiki at
     https://gitlab.com/tecnicosb/tsb-es/boatcomm/wikis/TSB-Protocol
    """
    size = len(motor_buffer)
    tsb.Received_addr = motor_buffer[1]

    if motor_buffer[2] == 0:
        tsb.heartbeat = 1
        tsb.last_heartbeat = timer()

    if motor_buffer[2] == 1:
        data_size = int((size - 6) / 4)

        if size<17:
            return -1
        data = np.ndarray(shape=(3,), dtype='>i', buffer=motor_buffer[3:15])
        tsb.avg_current_motor = float(data[0] / 100)
        tsb.avg_current_input = float(data[1] / 100)
        tsb.rpm = int(data[2] / 5 )


        data3 = np.ndarray(shape=(2,), dtype='>h', buffer=motor_buffer[15:19])
        tsb.voltage_motor = float(data3[0] / 10)
        tsb.temp_mos1 = float(data3[1] / 10)
        tsb.aux_temp1 = data3[1]

        lcd.power_input_motor = abs(float(tsb.avg_current_input*tsb.voltage_motor))
        tsb.power_input= lcd.power_input_motor
        lcd.rpm = data[2]
        
    if motor_buffer[2] == 2:
        data_size = int((size - 6) / 4)

        data = np.ndarray(shape=(2,), dtype='>i', buffer=motor_buffer[3:11])
        tsb.avg_current_motor2 = float(data[0] / 100)
        tsb.avg_current_input2 = float(data[1] / 100)
        #tsb.rpm = int(data[2] / 5 )


        data3 = np.ndarray(shape=(1,), dtype='>h', buffer=motor_buffer[11:13])
        #tsb.voltage_motor = float(data3[0] / 10)
        tsb.temp_mos2 = float(data3[0] / 10)
        tsb.aux_temp2 = float(data3[0] / 10)
        #lcd.temp_mos = data3[1]
    if tsb.temp_mos1 != -1 or tsb.temp_mos2 !=-1:
        if tsb.temp_mos1 > tsb.temp_mos2:
            lcd.temp_mos = tsb.aux_temp1
        else:
            lcd.temp_mos = tsb.aux_temp2


    motor = (tsb.heartbeat, tsb.avg_current_motor,tsb.avg_current_input,tsb.voltage_motor,\
             tsb.rpm,tsb.temp_mos1,tsb.power_input,tsb.avg_current_motor2,tsb.avg_current_input2,tsb.temp_mos2)
    print(motor)
    lcd_table = (lcd.power_input_motor,lcd.rpm,lcd.temp_mos)
    if -1 not in lcd_table:
        print("LCD upload")
        print("Sending lcd")
        lcd_msg(lcd_table, motor_buffer[1] + 1)
        #a
        lcd.power_input_motor = lcd.rpm = lcd.temp_mos=-1
    if -1 not in motor:
        # Send
        # print("motor upload")
        print(motor)
        cur.execute("INSERT INTO Motor (Heartbeat,CurrentMotor,CurrentInput,VoltInput, RPM,MosTemp1,PowerInput,CurrentMotor2,CurrentInput2,MosTemp2) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);",motor)

        tsb.avg_current_motor= tsb.avg_current_input= tsb.voltage_motor= \
        tsb.throttle_current= tsb.rpm= tsb.temp_mos1 = tsb.power_input=-1
        tsb.avg_current_motor2 = tsb.avg_current_input2 = tsb.temp_mos2 = -1

def decode_foils_msg(foils_buffer):
    """Act according to the message received

    Check the important information contained on the message and executes the
    code specific for each message, according to the tables defined on the project wiki
    for the Foil Controller
    Args:
        buf: The message returned from the receive_msg() function

    Returns:

        0 if the function succeeds

     For further information on how each message is handled consult the wiki at
     https://gitlab.com/tecnicosb/tsb-es/boatcomm/wikis/TSB-Protocol
    """
    size = len(foils_buffer)
    tsb.Received_addr = foils_buffer[1]
    print(foils_buffer[2])
    if foils_buffer[2] == 0:
        tsb.heartbeat = 1
        tsb.last_heartbeat = timer()

    if foils_buffer[2] == 1:
        data_size = int((size - 5) / 2)
        data = np.ndarray(shape=(data_size,), dtype='<H', buffer=foils_buffer[3:size - 2])
        tsb.dist_left = float(data[0] * 0.00859536)
        tsb.dist_right = float(data[1] * 0.00859536)

    if foils_buffer[2] == 2:
        data_size = int((size - 5) / 4)
        data = np.ndarray(shape=(data_size,), dtype='<i', buffer=foils_buffer[3:size - 2])
        tsb.yaw = struct.unpack('!f', data[0])
        tsb.pitch = struct.unpack('!f', data[1])
        tsb.roll = struct.unpack('!f', data[2])


    if foils_buffer[2] == 3:
        data_size = int((size - 5) / 4)
        data = np.ndarray(shape=(data_size,), dtype='<i', buffer=foils_buffer[3:size - 2])
        tsb.accX = struct.unpack('!f', data[0])
        tsb.accY = struct.unpack('!f', data[1])
        tsb.accZ = struct.unpack('!f', data[2])

    if foils_buffer[2] == 4:
        data_size = int((size - 5) / 4)
        data = np.ndarray(shape=(data_size,), dtype='<i', buffer=foils_buffer[3:size - 2])
        tsb.gyrX = struct.unpack('!f', data[0])
        tsb.gyrY = struct.unpack('!f', data[1])
        tsb.gyrZ = struct.unpack('!f', data[2])

    if foils_buffer[2] == 5:
        data_size = int((size - 5) / 4)
        data = np.ndarray(shape=(data_size,), dtype='<i', buffer=foils_buffer[3:size - 2])
        tsb.lat = struct.unpack('!f', data[0])
        tsb.lon= struct.unpack('!f', data[1])


    if foils_buffer[2] == 6:
        data_size = int((size - 5) / 4)
        data = np.ndarray(shape=(data_size,), dtype='<i', buffer=foils_buffer[3:size - 2])
        tsb.velX = struct.unpack('!f', data[0])
        tsb.velY = struct.unpack('!f', data[1])
        tsb.velZ = struct.unpack('!f', data[2])
        lcd.velX = data[0]
        lcd.velY = data[1]

    if foils_buffer[2] == 7:
        data_size = int((size - 5) / 2)
        data = np.ndarray(shape=(data_size,), dtype='<h', buffer=foils_buffer[3:size - 2])
        tsb.alpha_left = data[0] * 1000
        tsb.alpha_right = data[1] * 1000
        tsb.alpha_rear = data[2] * 1000

    foils = (tsb.heartbeat, tsb.dist_left, tsb.dist_right, tsb.yaw, tsb.pitch, tsb.roll, \
             tsb.accX, tsb.accY, tsb.accZ, tsb.gyrX, tsb.gyrY, tsb.gyrZ, tsb.lat, tsb.lon, tsb. \
             velX, tsb.velY, tsb.velZ, tsb.alpha_left, tsb.alpha_right, tsb.alpha_rear)
    lcd_table = (lcd.velX, lcd.velY)
    #print(foils)
    if -0.1234 not in foils:
        #print(foils)
        cur.execute("INSERT INTO Foils (Heartbeat,DistLeft, DistRight,Yaw, Pitch,Roll,accX, accY,accZ,\
         gyrX, gyrY,gyrZ,lat, lon,velX, velY,velZ,Alpha_Left, Alpha_Right,Alpha_Rear) VALUES (%s,%s,%s,%s,\
         %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);", foils)
        tsb.dist_left = tsb.dist_right=tsb.yaw = tsb.pitch = tsb.roll = tsb.accX = tsb.accY = tsb.accZ = tsb.gyrX \
            = tsb.gyrY = tsb.gyrZ = tsb.lat = tsb.lon = tsb.velX = tsb.velY = tsb.velZ = tsb.alpha_left \
            = tsb.alpha_right = tsb.alpha_rear = -0.1234

        tsb.alpha_left=tsb.alpha_right=tsb.alpha_rear= -500
        tsb.dist_left=tsb.dist_right= -500

    if -1 not in lcd_table:
        print("LCD upload")
        print("Sending lcd")
        #lcd_msg(lcd_table, foils_buffer[1] + 1)

        lcd.velX = lcd.velY = -1


def decode_bss_msg(bss_buffer):
    """Act according to the message received

    Check the important information contained on the message and executes the
    code specific for each message, according to the tables defined on the project wiki
    for the Battery Controller

    Args:
        buf: The message returned from the receive_msg() function

    Returns:

        0 if the function succeeds

     For further information on how each message is handled consult the wiki at
     https://gitlab.com/tecnicosb/tsb-es/boatcomm/wikis/TSB-Protocol
    """
    size = len(bss_buffer)
    tsb.Received_addr = bss_buffer[1]
    # print(bss_buffer[2])
    if bss_buffer[2] == 0:
        # print("Bss0")
        tsb.heartbeat = 1
        tsb.last_heartbeat = timer()

    if bss_buffer[2] == 1:
        # print("Bss1")
        data_size = int((size - 6) / 4)
        data = np.ndarray(shape=(2,), dtype='>I', buffer=bss_buffer[8:16])
        data3 = np.ndarray(shape=(1,), dtype='<i', buffer=bss_buffer[4:8])
        status = bss_buffer[3]
        if status == 145:
            tsb.status = "Charging"
        if status == 146:
            tsb.status = "Full"
        if status == 147:
            tsb.status = "Discharging"
        if status == 149:
            tsb.status = "Sleep"
        if status == 150:
            tsb.status = "Regeneration"
        if status == 151:
            tsb.status = "Idle"
        if status == 155:
            tsb.status = "Fault"

        tsb.soc = float(data3[0] / 1000000)
        tsb.voltage_bat = struct.unpack('!f', data[0])
        tsb.current_bat = struct.unpack('!f', data[1])
        lcd.bat_power = float(tsb.current_bat[0])
        lcd.bat_voltage = float(tsb.voltage_bat[0])
        lcd.status = float(bss_buffer[3])
        solar = np.ndarray(shape=(1,), dtype='<H', buffer=bss_buffer[16:18])
        lcd.solar_power = float(solar[0])
        tsb.solar = int(solar[0])
        lcd.warnings = bss_buffer[18]
        tsb.warning = bss_buffer[18]

    if bss_buffer[2] == 2:
        data_size = int((size - 5))
        data = np.ndarray(shape=(data_size,), dtype='>b', buffer=bss_buffer[3:size - 2])
        tsb.t_1 = int(data[0])
        tsb.t_2 = int(data[1])
        tsb.t_3 = int(data[2])
        tsb.t_4 = int(data[3])
        tsb.t_5 = int(data[4])
        tsb.t_6 = int(data[5])
        tsb.t_7 = int(data[6])
        tsb.t_8 = int(data[7])
        tsb.t_9 = int(data[8])
        tsb.t_10 = int(data[9])
        tsb.t_11 = int(data[10])
        tsb.t_12 = int(data[11])
        tsb.t_13 = int(data[12])
        tsb.t_14 = int(data[13])
        tsb.t_15 = int(data[14])
        tsb.t_16 = int(data[15])

    if bss_buffer[2] == 3:
        data_size = int((size - 5))
        data = np.ndarray(shape=(data_size,), dtype='>b', buffer=bss_buffer[3:size - 2])
        tsb.t_17 = int(data[0])
        tsb.t_18 = int(data[1])
        tsb.t_19 = int(data[2])
        tsb.t_20 = int(data[3])
        tsb.t_21 = int(data[4])
        tsb.t_22 = int(data[5])
        tsb.t_23 = int(data[6])
        tsb.t_24 = int(data[7])
        tsb.t_25 = int(data[8])
        tsb.t_26 = int(data[9])
        tsb.t_27 = int(data[10])
        tsb.t_28 = int(data[11])
        tsb.t_29 = int(data[12])
        tsb.t_30 = int(data[13])
        tsb.t_31 = int(data[14])
        tsb.t_32 = int(data[15])

    if bss_buffer[2] == 4:
        data_size = int((size - 5) / 2)
        data = np.ndarray(shape=(data_size,), dtype='<H', buffer=bss_buffer[3:size - 2])
        tsb.v1 = float(data[0] / 10)
        tsb.v2 = float(data[1] / 10)
        tsb.v3 = float(data[2] / 10)
        tsb.v4 = float(data[3] / 10)
        tsb.v5 = float(data[4] / 10)
        tsb.v6 = float(data[5] / 10)

    if bss_buffer[2] == 5:
        data_size = int((size - 5) / 2)
        data = np.ndarray(shape=(data_size,), dtype='<H', buffer=bss_buffer[3:size - 2])
        tsb.v7 = float(data[0] / 10)
        tsb.v8 = float(data[1] / 10)
        tsb.v9 = float(data[2] / 10)
        tsb.v10 = float(data[3] / 10)
        tsb.v11 = float(data[4] / 10)
        tsb.v12 = float(data[5] / 10)

    if bss_buffer[2] == 6:
        data_size = int((size - 5) / 2)
        data = np.ndarray(shape=(data_size,), dtype='<H', buffer=bss_buffer[3:size - 2])
        tsb.v_p1 = float(data[0] * 10)
        tsb.v_p2 = float(data[1] * 10)
        tsb.v_p3 = float(data[2] * 10)
        tsb.v_p4 = float(data[3] * 10)
        tsb.v_p5 = float(data[4] * 10)
        # print(tsb.v_p1, tsb.v_p2, tsb.v_p3, tsb.v_p4, tsb.v_p5)

    bss = (tsb.heartbeat, tsb.status, tsb.soc, tsb.voltage_bat, tsb.current_bat, \
           tsb.v1, tsb.v2, tsb.v3, tsb.v4, tsb.v5, tsb.v6, tsb.v7, tsb.v8, tsb.v9, \
           tsb.v10, tsb.v11, tsb.v12, tsb.t_1, tsb.t_2, tsb.t_3, tsb.t_4, tsb.t_5, tsb.t_6, tsb.t_7, tsb.t_8,
           tsb.t_9, \
           tsb.t_10, tsb.t_11, tsb.t_12, tsb.t_13, tsb.t_14, tsb.t_15, tsb.t_16, tsb.t_17, tsb.t_18, tsb.t_19, \
           tsb.t_20, tsb.t_21, tsb.t_22, tsb.t_23, tsb.t_24, tsb.t_25, tsb.t_26, tsb.t_27, tsb.t_28, tsb.t_29,
           tsb.t_30, \
           tsb.t_31, tsb.t_32, tsb.warning, tsb.solar, tsb.v_p1, tsb.v_p2, tsb.v_p3, tsb.v_p4, tsb.v_p5)
    # print(bss)
    lcd_table = (lcd.bat_voltage, lcd.bat_power, lcd.solar_power, lcd.status)
    #print(lcd_table)
    if -1 not in bss:
        print("BSS upload")
        cur.execute("INSERT INTO BSS (Heartbeat,BMS_Status,SOC,Voltage,Current,\
              V1,V2,V3,V4,V5,V6,V7,V8,V9,V10,V11,V12,T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,\
              T13,T14,T15,T16,T17,T18,T19,T20,T21,T22,T23,T24,T25,T26,T27,T28,T29,T30,T31,T32,Warnings,\
              V_Painel1,V_Painel2,V_Painel3,V_Painel4,V_Painel5\
              ,SOLAR) VALUES (%s,%s,%s,%s,\
              %s,%s,%s,%s,%s,%s,%s,%s,\
              %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,\
              %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);", bss)

        tsb.soc = tsb.voltage_bat = tsb.current_bat = tsb.bms_sensor = tsb.ext1_sensor = \
            tsb.ext2_sensor = -1
        tsb.v1 = tsb.v2 = tsb.v3 = tsb.v4 = tsb.v5 = tsb.v6 = tsb.v7 = tsb.v8 = tsb.v9 = tsb.v10 = \
            tsb.v11 = tsb.v12 = -1
        tsb.t_1 = tsb.t_2 = tsb.t_3 = tsb.t_4 = tsb.t_5 = tsb.t_6 = tsb.t_7 = tsb.t_8 = tsb.t_9 = tsb.t_10 = \
            tsb.t_11 = tsb.t_12 = tsb.t_13 = tsb.t_14 = tsb.t_15 = tsb.t_16 = tsb.t_17 = tsb.t_18 = tsb.t_19 = \
            tsb.t_20 = tsb.t_21 = tsb.t_22 = tsb.t_23 = tsb.t_24 = tsb.t_25 = tsb.t_26 = tsb.t_27 = tsb.t_28 = \
            tsb.t_29 = tsb.t_30 = tsb.t_31 = tsb.t_32 = -1
        tsb.v_p1 = tsb.v_p2 = tsb.v_p3 = tsb.v_p4 = tsb.v_p5 = -1

    if -1 not in lcd_table:
        print("LCD upload")
        print("Sending lcd")
        lcd_msg(lcd_table, bss_buffer[1] + 1)

        lcd.bat_voltage = lcd.bat_power = lcd.solar_power = lcd.status = lcd.warnings = -1


# Execution
time_msg()

while True:
    try:
        message = receive_msg()
    #     #print(message)
        deal_with_message(message)
        current_time=timer()
        if (current_time-tsb.last_heartbeat) > 15:
            tsb.heartbeat = 10
    
    except:
        port.close()
        sys.exit(-1)

    #message = receive_msg()
    #deal_with_message(message)

