'''
    Copyright (C) 2018  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat 
'''

# Variables for packing database data
# Has a similar effect as global variables in C
# The default value is 0

# Motor
Received_addr = 0
asked_current_M0 = asked_current_M1 = asked_throttle = turning_dir = motor_oper = master_motor = current_M0 = 10
current_M1 = speed_M0 = speed_M1 = temp_M0 = temp_M1 = bat_volt_M0 = 2
bat_volt_M1 = motor_state_M0 = drive_ready_M0 = fail_safe_M0 = motor_state_M1 = drive_ready_M1 = fail_safe_M1 = 2
motor_warnings=0
power_input=0

avg_current_motor= avg_current_input= voltage_motor  = 0
rpm = temp_mos1 = temp_mos2 = temp_mos3 = temp_mos4 = temp_mos5 = temp_mos6 = controller_pcb_temp = 0
avg_current_motor2 = avg_current_input2 = temp_mos_m2 = 0
aux_temp1 = 0
aux_temp2 = 0
# Foils
dist_left = dist_right= -500
yaw = pitch = roll= accX = accY = accZ = gyrX = gyrY = gyrZ\
=lat = lon = velX = velY = velZ = -0.1234
alpha_left = alpha_right = -500
alpha_rear= -500

heartbeat=10

#BSS default value is -1
soc = voltage_bat = current_bat = bms_sensor = ext1_sensor = ext2_sensor = -1
v1 = v2 = v3 = v4 =v5 =v6 =v7 =v8 =v9 =v10 =v11 =v12  =-1
t_1 = t_2 = t_3 = t_4 = t_5 = t_6 = t_7 = t_8 = t_9 = t_10 = t_11 = t_12 = \
t_13 = t_14 = t_15 = t_16 = t_17 = t_18 = t_19 = t_20 = t_21 = t_22 = t_23 = t_24 = \
t_25 = t_26 = t_27 = t_28 = t_29 = t_30 = t_31 = t_32  = -1
solar=-1
status= ""
warning = -1
last_heartbeat=0
v_p1=v_p2=v_p3=v_p4=v_p5=-1