/*
    Copyright (C) 2018  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

// the setup function runs once when you press reset or power the board
// To use VescUartControl stand alone you need to define a config.h file, that should contain the Serial or you have to comment the line
// #include Config.h out in VescUart.h

//Include libraries copied from VESC
 #include "VescUart.h"
#include "datatypes.h"
#include "mctrl_torquedo.h"
#include <TSB_datalogger.h>
#include <tsb_serial.h>
#include "Config.h"

#define Waiting_time 180000 //time waiting for RPI time message
#define Current_switch_threshold 25
#define HB_TIME 5
int time_flag=0;


unsigned long count;
float inputcurrent = 0;
int killswitch_flag = 0;

IntervalTimer Motor_timer;
TSB_TORQUEDO MCTRL_THROTTLE_class;
TSB_SERIAL tsb_serial_class;
tsb_serial_msg_t tsb_msg;


void send_Heartbeat(){
  TSB_SERIAL tsb_serial_class;
  tsb_serial_msg_t message_HeartBeat;
  message_HeartBeat.addr=0x01;
  message_HeartBeat.data_id=0x00;
  message_HeartBeat.data_size=0x00;
  if(Serial) tsb_serial_class.send_msg(message_HeartBeat);
}


void setup() {
	SetSerialPort(&MOTOR1, &MOTOR2);
	MOTOR1.begin(115200);
	MOTOR2.begin(115200);
    SetDebugSerialPort(&DEBUGSERIAL); //DEBUGSERIAL is the RPI serial
    DEBUGSERIAL.begin(115200);
    Serial1.begin(19200);
    pinMode(13, OUTPUT);
    pinMode(42, INPUT_PULLDOWN);

    digitalWrite(13, HIGH);

    MCTRL_THROTTLE_class.PrepareThrottleRequest();

    
    int initial_time=millis();
    while(Serial.available() <= 0 && time_flag==0){
        if(millis()-initial_time > Waiting_time) time_flag=1;
    }
    if(time_flag==0){
        tsb_msg=tsb_serial_class.receive_msg();
        setTime(tsb_msg.data[0],tsb_msg.data[1],tsb_msg.data[2],tsb_msg.data[3],tsb_msg.data[4],2000+tsb_msg.data[5]);
    } 
    
    initSD();
    initLOG();
    Motor_timer.begin(send_Heartbeat, HB_TIME*1000000);
    while(MCTRL_THROTTLE_class.getDesiredCurrent()!=0){}

}

struct bldcMeasure measuredValues;
struct bldcMeasure measuredValues2;
	
void loop() {
    digitalWrite(13,  !digitalRead(13));
    //Serial.println(MCTRL_THROTTLE_class.getDesiredCurrent());
    if( digitalRead(42) == 0){
        killswitch_flag = 1;
        VescUartSetCurrent(0,0);
        VescUartSetCurrent(0,1);
    } 
    switch(killswitch_flag){
        case 0:
            switch(reverse_flag){
                case 0:
                    if(abs(MCTRL_THROTTLE_class.getDesiredCurrent())>= Current_switch_threshold){
                    VescUartSetCurrent((MCTRL_THROTTLE_class.getDesiredCurrent()/2),0);
                    VescUartSetCurrent((MCTRL_THROTTLE_class.getDesiredCurrent()/2),1);
                    } 
                    else VescUartSetCurrent(MCTRL_THROTTLE_class.getDesiredCurrent(),0);
                    break;
                case 1:
                    VescUartSetCurrent(MCTRL_THROTTLE_class.getDesiredCurrent(),1);
                    break;
            } 
            break;           
        case 1:
            if(MCTRL_THROTTLE_class.getDesiredCurrent()==0){
                killswitch_flag = 0;
            }
            break;
    }

    VescUartGetValue(measuredValues2,1);
    VescUartGetValue(measuredValues,0);
    Data=createMotorData(measuredValues);
    writeLogEntry();
    init_messages();
    if(Serial) send_messages();
    
    
    
    
    

    //SerialPrint(measuredValues2); //debugging only
	
}




