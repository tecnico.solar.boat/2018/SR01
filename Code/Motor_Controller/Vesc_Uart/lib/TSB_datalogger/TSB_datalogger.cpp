/*
    Copyright (C) 2018  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat 
*/


/*
 * TSB
 * Code for the SD data logger 
 * for every eletrical system
 * 
 */

#include "TSB_datalogger.h"

SdFatSdio sd;
SdFatSdioEX sdEx;
String Data="";
int entryId = 0;
String timeString;
String dateString;
String logname;
IntervalTimer SD_timer;
File file;
bool sd_insert = true;

void getTime()
{
  String minutes;
  String hours;
  String seconds;
  String month_;
  String mday;
  
  if(minute()<10) minutes = "0"+String(minute());
  else minutes = String(minute());
  
  if(second()<10) seconds = "0"+String(second());
  else seconds = String(second());

  if(hour()<10) hours = "0"+String(hour());
  else hours = String(hour());

  if(month()<10) month_ = "0"+String(month());
  else month_ = String(month());

  if(day()<10) mday = "0"+String(day());
  else mday = String(day());

  timeString = hours+":"+minutes+":"+seconds;
  dateString = mday+"/"+month_+"/"+String(year());
}

 void writeLogEntry()
{
  if (sd_insert == false) return;
  file.open(logname.c_str(),  O_WRITE | O_AT_END);
  String logEntry;
  getTime();
  logEntry = String(entryId)+";"+timeString+";"+dateString+";"+Data;
  entryId ++;
  file.println(logEntry);
  file.close();
} 

void initLOG()
{
  if (sd_insert == false) return;
  file.open(logname.c_str(), O_WRITE | O_CREAT | O_AT_END );
  /*file.println("----------------Data logging----------------");
  file.println(String(System_Name)+" at "+timeString+"-"+dateString);
  file.println("--------------------------------------------");*/
  file.println("Motor;Time;Date;Motor_curr;Input_curr;Volt_input;RPM;Temperature");
  file.close();
}


void initSD(){
   if (useEX) {
    if (!sdEx.begin()) {
      sd_insert = false;
    }
    else{
      sdEx.chvol();
      getTime();
      logname=System_Name+String(day())+"_"+String(month())+"-"+String(hour())+"_"+String(minute())+".txt";
    
    }
    
  } else {
    if (!sd.begin()) {
      sd_insert = false;
    }
    else{
      sd.chvol();
      getTime();
      logname=System_Name+String(day())+"_"+String(month())+"-"+String(hour())+"_"+String(minute())+".txt";
    }
  
  }
 
}